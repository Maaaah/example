package ru.example;

import java.nio.ByteBuffer;

public class Buffer {
    public static ByteBuffer slice(int start, int end, ByteBuffer source) {
        int limit = source.limit();
        int position = source.position();

        source.clear();

        source.limit(end);
        source.position(start);

        ByteBuffer result = source.slice();

        source.clear();

        source.limit(limit);
        source.position(position);

        return result;
    }
}
