package ru.example;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;

public interface Processor {
    public void process(Worker.Context context, RegularRequest request, ByteBuffer buffer) throws Exception;
}