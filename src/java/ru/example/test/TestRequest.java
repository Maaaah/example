package ru.example.test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.nio.ByteBuffer;
import java.util.Formatter;

import org.junit.Assert;
import org.junit.Test;
import ru.example.RegularRequest;
import ru.example.URI;

public class TestRequest {
    private static final byte[] EOF;

    static {
        EOF = new byte[0];
    }

    private static enum Protocol {
        HTTP10("HTTP/1.0"), HTTP11("HTTP/1.1");

        private final String protocol;

        private Protocol(String protocol) {
            this.protocol = protocol;
        }

        public String toString() {
            return protocol;
        }
    }

    private static enum Method {GET, POST}

    private static enum Mode {
        HTTP10(false, false, true, false),
        KEEPALIVE(true, false, false, false),
        KEEPALIVE_EOF(true, false, true, false),
        KEEPALIVE_SEPARATE_EOF(true, false, true, true),
        PIPELINE(true, true, false, false), // Requests are pipelined, no EOF in the end of the stream, last request is indicated by "Connection: close"
        PIPELINE_EOF(true, true, true, false), // Requests are pipelined, EOF in the end of the stream, read together with the last request
        PIPELINE_SEPARATE_EOF(true, true, true, true); // Requests are pipelined, EOF in the end of the stream, but read only after the whole stream is read

        private final boolean isKeepalive;
        private final boolean isPipeline;
        private final boolean isEOF;
        private final boolean isSeparateEOF;

        private Mode(boolean isKeepalive, boolean isPipeline, boolean isEOF, boolean isSeparateEOF) {
            if ((!isKeepalive && isPipeline) || (isSeparateEOF && !isEOF)) {
                throw new IllegalArgumentException();
            }

            this.isKeepalive = isKeepalive;
            this.isPipeline = isPipeline;
            this.isEOF = isEOF;
            this.isSeparateEOF = isSeparateEOF;
        }

        public boolean isKeepalive() {
            return isKeepalive;
        }

        public boolean isPipeline() {
            return isPipeline;
        }

        public boolean isEOF() {
            return isEOF;
        }

        public boolean isSeparateEOF() {
            return isSeparateEOF;
        }

        public MockReadableByteChannel makeChannel(byte[][] requests, int portion) {
            byte[][] data = requests;

            if (isPipeline) {
                data = Util.join(data);
            }

            if (isSeparateEOF) {
                data = Util.append(data, EOF);
            }

            return new MockReadableByteChannel(data, portion, isEOF());
        }
    }

    private static class RequestData {
        private final Method method;
        private final String URI;
        private final String decodedURI;
        private final byte[] body;
        private byte[] data;
        private String connectionCloseHeader;
        private String contentLengthHeader;

        private RequestData(Method method, String URI, byte[] body)
                throws UnsupportedEncodingException {
            this(method, URI, body, "Connection: close", "Content-Length: %d");
        }

        private RequestData(Method method, String URI, String decodedURI, byte[] body)
                throws UnsupportedEncodingException {
            this(method, URI, decodedURI, body, "Connection: close", "Content-Length: %d");
        }

        private RequestData(Method method, String URI, byte[] body,
                            String connectionCloseHeader, String contentLengthHeader) {
            this(method, URI, URI, body, connectionCloseHeader, contentLengthHeader);

            if (URI.indexOf('%') >= 0) {
                throw new IllegalArgumentException();
            }
        }

        private RequestData(Method method, String URI, String decodedURI, byte[] body,
                            String connectionCloseHeader, String contentLengthHeader) {
            this.method = method;
            this.URI = URI;
            this.decodedURI = decodedURI;
            this.body = body;
            this.connectionCloseHeader = connectionCloseHeader;
            this.contentLengthHeader = contentLengthHeader;
        }

        private byte[] makeData(Protocol protocol, boolean connectionClose) throws UnsupportedEncodingException {
            boolean hasBody = (body != null);
            int bodyLength = 0;
            if (hasBody) {
                bodyLength = body.length;
            }

            Formatter formatter = new Formatter();
            formatter.format(contentLengthHeader, bodyLength);

            byte[] requestLineAndHeaders = (method + " " + URI + " " + protocol + "\r\n"
                    + "Accept: */*\r\n"
                    + "Test: 1:?3"
                    + "Accept-Encoding: gzip, deflate\r\n"
                    + "User-Agent: Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)\r\n"
                    + "Host: test.host.com\r\n"
                    + (connectionClose ? connectionCloseHeader + "\r\n" : "")
                    + (hasBody ? formatter.out() + "\r\n" : "")
                    + "\r\n").getBytes("ISO-8859-1");

            int requestLineAndHeadersLength = requestLineAndHeaders.length;

            byte[] data = new byte[requestLineAndHeadersLength + bodyLength];

            System.arraycopy(requestLineAndHeaders, 0, data, 0, requestLineAndHeadersLength);

            if (hasBody) {
                System.arraycopy(body, 0, data, requestLineAndHeadersLength, bodyLength);
            }

            return data;
        }
    }

    private void checkRequest(RequestData requestData, RegularRequest request, String test)
            throws MalformedURLException, UnsupportedEncodingException {
        if (requestData.method == Method.GET) {
            Assert.assertTrue(test, request.isGet());
            Assert.assertFalse(test, request.isPost());
        } else {
            Assert.assertFalse(test, request.isGet());
            Assert.assertTrue(test, request.isPost());
        }

        Assert.assertFalse(test, request.isOverflow());
        Assert.assertFalse(test, request.isError());

        URI URI = new URI(request.URI());
        StringBuilder URIString = new StringBuilder();
        while (URI.hasNext()) {
            URIString.append(URI.next());
        }
        Assert.assertEquals(test, requestData.decodedURI, URIString.toString());

        ByteBuffer body = request.body();
        if (requestData.body == null) {
            Assert.assertNull(body);
        } else {
            byte[] bodyBytes = new byte[requestData.body.length];
            Assert.assertTrue(test, body.remaining() >= bodyBytes.length);
            body.get(bodyBytes);
            Assert.assertArrayEquals(test, requestData.body, bodyBytes);
            Assert.assertFalse(body.remaining() > 0);
        }
    }

    private void testRequest(RequestData requestData)
            throws IOException {
        testRequests(new RequestData[]{requestData});
    }

    private void testRequests(RequestData[] requestsData)
            throws IOException {
        Mode[] modes = null;
        if (requestsData.length == 1) {
            modes = new Mode[]{Mode.HTTP10, Mode.KEEPALIVE, Mode.KEEPALIVE_EOF,
                    Mode.KEEPALIVE_SEPARATE_EOF, Mode.PIPELINE, Mode.PIPELINE_EOF,
                    Mode.PIPELINE_SEPARATE_EOF};
        } else {
            modes = new Mode[]{Mode.KEEPALIVE, Mode.KEEPALIVE_EOF, Mode.KEEPALIVE_SEPARATE_EOF,
                    Mode.PIPELINE, Mode.PIPELINE_EOF, Mode.PIPELINE_SEPARATE_EOF};
        }

        for (Mode mode : modes) {
            testRequests(requestsData, mode);
        }
    }

    private void testRequests(RequestData[] requestsData, Mode mode)
            throws IOException

    {
        int requestCount = requestsData.length;
        if (mode == Mode.HTTP10 && requestCount > 1) {
            throw new IllegalArgumentException();
        }

        int minBufferSize = 0;
        byte[][] requests = new byte[requestCount][];
        for (int i = 0; i < requestCount; i++) {
            byte[] data = requestsData[i].makeData(
                    mode == Mode.HTTP10 ? Protocol.HTTP10 : Protocol.HTTP11,
                    (mode == Mode.PIPELINE || mode == Mode.KEEPALIVE) && (i == requestCount - 1));

            requests[i] = data;

            minBufferSize = Math.max(minBufferSize, data.length);
        }

        boolean isSeparateEOF = mode.isSeparateEOF();
        boolean isEOF = mode.isEOF();

        for (int i = minBufferSize; i <= 2 * minBufferSize + 1; i++) {
            int[] portions = {1, minBufferSize / 2, minBufferSize, 1024};

            for (int portion : portions) {
                MockReadableByteChannel channel = mode.makeChannel(requests, portion);

                testRequests(requestsData, channel, mode, portion, i);
            }
        }
    }


    private void testRequests(RequestData[] requestsData, MockReadableByteChannel channel, Mode mode,
                              int portion, int bufferSize)
            throws IOException

    {
        String test = "mode: " + mode + ", portion: " + portion + ", buffer size: " + bufferSize;

        ByteBuffer buffer = ByteBuffer.allocate(bufferSize);

        RegularRequest request = new RegularRequest(buffer);

        int requestCount = requestsData.length;

        int readsRequired = channel.readsToFullyRead();

        boolean done = false;
        int requestsRead = 0;
        int readsMade = 0;
        while (readsMade < readsRequired + requestCount - 1) {
            done = request.read(channel);

            readsMade += request.readsMade();

            if (done) {
                if (requestsRead < requestCount) {
                    checkRequest(requestsData[requestsRead], request, test);
                }

                requestsRead++;

                if (mode != Mode.HTTP10) {
                    Assert.assertTrue(test, request.isHTTP11());
                } else {
                    Assert.assertFalse(test, request.isHTTP11());
                }

                boolean isLast = request.isLast();

                if (!mode.isPipeline()) {
                    Assert.assertFalse(test, request.hasPendingData());
                }

                request.reset();

                channel.proceed();

                if (requestsRead == requestCount) {
                    if (mode.isSeparateEOF()) {
                        Assert.assertFalse(test, isLast);
                    } else {
                        Assert.assertTrue(test, isLast);
                    }

                    break;
                } else {
                    Assert.assertFalse(test, isLast);
                }
            }
        }

        if (mode.isSeparateEOF()) {
            done = request.read(channel);

            Assert.assertFalse(test, done);
            Assert.assertFalse(test, request.isError());
        }

        if (mode.isEOF()) {
            Assert.assertTrue(test, request.isEOF());
        } else {
            Assert.assertFalse(test, request.isEOF());
        }

        done = request.read(channel);

        Assert.assertFalse(test, done);

        Assert.assertEquals(test + " " + readsRequired, requestCount, requestsRead);
    }

    @Test
    public void testDoubleGet() throws IOException {
        testRequests(new RequestData[]{
                new RequestData(Method.GET, "/favicon.ico", null),
                new RequestData(Method.GET, "/favicon.ico", null)
        });
    }

    @Test
    public void testFourGet() throws IOException {
        testRequests(new RequestData[]{
                new RequestData(Method.GET, "/favicon.ico", null),
                new RequestData(Method.GET, "/favicon.ico", null),
                new RequestData(Method.GET, "/favicon.ico?p=1", null),
                new RequestData(Method.GET, "/favicon.ico", null)
        });
    }

    @Test
    public void testDoublePost() throws IOException {
        testRequests(new RequestData[]{
                new RequestData(Method.POST, "/favicon.ico", "BODY".getBytes("ISO-8859-1")),
                new RequestData(Method.POST, "/favicon.ico", "BODY?".getBytes("ISO-8859-1"))
        });
    }

    @Test
    public void testFourPost() throws IOException {
        testRequests(new RequestData[]{
                new RequestData(Method.POST, "/favicon.ico", "BODY".getBytes("ISO-8859-1")),
                new RequestData(Method.POST, "/favicon.ico?p=1", "BODY BODY".getBytes("ISO-8859-1")),
                new RequestData(Method.POST, "/favicon.ico?p=1%3f3", "/favicon.ico?p=1?3", "BODY".getBytes("ISO-8859-1")),
                new RequestData(Method.POST, "/favicon.ico?p=%2A", "/favicon.ico?p=*", "BODY".getBytes("ISO-8859-1"))
        });
    }

    @Test
    public void testGetGetPost() throws IOException {
        testRequests(new RequestData[]{
                new RequestData(Method.GET, "/", null),
                new RequestData(Method.GET, "/", null),
                new RequestData(Method.POST, "/", "BODY BODY BODY".getBytes("ISO-8859-1")),
        });
    }

    @Test
    public void testSingleGet() throws IOException {
        testRequest(new RequestData(Method.GET, "/favicon.ico", null));
    }

    @Test
    public void testSinglePost() throws IOException {
        testRequest(new RequestData(Method.POST, "/favicon.ico", "BODY".getBytes("ISO-8859-1")));
    }

    @Test
    public void testSingleLongPost() throws IOException {
        testRequest(new RequestData(Method.POST, "/favicon.ico",
                ("GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY "
                        + "GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY GIANT BODY").getBytes("ISO-8859-1")));
    }

    @Test
    public void testSinglePostMutatedHeaders() throws IOException {
        testRequest(new RequestData(Method.POST, "/favicon.ico", "BODY".getBytes("ISO-8859-1"),
                "CONNECTION:   close ", "CONTENT-LENGTH:  %d\t "));
    }

    @Test
    public void testSinglePostMutatedHeaders2() throws IOException {
        testRequest(new RequestData(Method.POST, "/favicon.ico", "BODY".getBytes("ISO-8859-1"),
                "connection: close", "content-length: %d"));
    }

    @Test
    public void testMalformed() throws IOException {
        String[] patterns = {
                "MALFORMED\r\n",
                Method.GET + " MALFORMED\r\n",
                Method.GET + " MALFORMED " + Protocol.HTTP11 + " MALFO:RMED",
                Method.GET + " MALF:O:RMED T?:T\r\n\r\n",
                Method.POST + " MALFORMED " + Protocol.HTTP11 + "\r\nContent-Length: 1t\r\n\r\n",
                Method.POST + " MALFORMED " + Protocol.HTTP11 + "\r\nContent-Length: 1 t\r\n\r\n",
                Method.GET + " MALFORMED HTTP/2.0\r\n\r\n",
                Method.GET + " MALFORMED HTTP/1.2\r\n\r\n",
                Method.GET + " MALFORMED HTTP/1.10\r\n\r\n",
                Method.GET + " MALFORMED " + Protocol.HTTP11 + "\r\nContent-Length: 1\r\n\r\n",
                Method.POST + " MALFORMED " + Protocol.HTTP11 + "\r\n\r\n",
                "PUT MALFORMED HTTP/1.1\r\n\r\n",
                "HEAD MALFORMED HTTP/1.0\r\n\r\n",
                Method.GET + " MALFORMED " + Protocol.HTTP11 + "\r\n\rTest: test\r\n\r\n",
                Method.GET + " MALFORMED " + Protocol.HTTP11 + "\r\nTest: \rtest\r\n\r\n"
        };

        for (int i = 0; i < patterns.length; i++) {
            String test = ("pattern: " + i);

            byte[] data = patterns[i].getBytes("ISO-8859-1");

            MockReadableByteChannel channel
                    = new MockReadableByteChannel(new byte[][]{data}, 1024, false);

            ByteBuffer buffer = ByteBuffer.allocate(1024);

            RegularRequest request = new RegularRequest(buffer);

            int readsRequired = channel.readsToFullyRead();

            boolean done = false;
            int requestsRead = 0;
            int readsMade = 0;
            while (readsMade < readsRequired) {
                done = request.read(channel);

                readsMade += request.readsMade();

                if (done) {
                    break;
                }
            }

            Assert.assertTrue(test, done);
            Assert.assertTrue(test, request.isError());
        }
    }

    @Test
    public void testMalformed2() throws IOException {
        byte[] data = (Method.GET + " MALFORMED").getBytes("ISO-8859-1");

        MockReadableByteChannel channel
                = new MockReadableByteChannel(new byte[][]{data}, 1024, true);

        ByteBuffer buffer = ByteBuffer.allocate(1024);

        RegularRequest request = new RegularRequest(buffer);

        int readsRequired = channel.readsToFullyRead();

        boolean done = false;
        int requestsRead = 0;
        int readsMade = 0;
        while (readsMade < readsRequired) {
            done = request.read(channel);

            readsMade += request.readsMade();

            if (done) {
                break;
            }
        }

        Assert.assertTrue(done);
        Assert.assertTrue(request.isEOF());
        Assert.assertTrue(request.isError());
    }

    @Test
    public void testOverflow() throws IOException {
        byte[] data = (Method.GET + " OVERFLOW " + Protocol.HTTP11 + "\r\n\r\n").getBytes("ISO-8859-1");

        MockReadableByteChannel channel
                = new MockReadableByteChannel(new byte[][]{data}, 1024, true);

        ByteBuffer buffer = ByteBuffer.allocate(10);

        RegularRequest request = new RegularRequest(buffer);

        int readsRequired = channel.readsToFullyRead();

        boolean done = false;
        int requestsRead = 0;
        int readsMade = 0;
        while (readsMade < readsRequired) {
            done = request.read(channel);

            readsMade += request.readsMade();

            if (done) {
                break;
            }
        }

        Assert.assertTrue(done);
        Assert.assertTrue(request.isOverflow());
    }
}
