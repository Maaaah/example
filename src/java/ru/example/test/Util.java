package ru.example.test;

public class Util {
    public static byte[][] join(byte[][] data) {
        int totalLength = 0;
        for (byte[] chunk : data) {
            totalLength += chunk.length;
        }

        byte[] result = new byte[totalLength];
        int position = 0;
        for (byte[] chunk : data) {
            int length = chunk.length;
            System.arraycopy(chunk, 0, result, position, length);
            position += length;
        }

        return new byte[][]{result};
    }

    public static byte[][] append(byte[][] data, byte[] extra) {
        int length = data.length;
        byte[][] result = new byte[length + 1][];
        System.arraycopy(data, 0, result, 0, length);
        result[length] = extra;

        return result;
    }
}
