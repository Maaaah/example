package ru.example.test;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

public class MockReadableByteChannel implements ReadableByteChannel {
    private final byte[][] data;
    private final int readsToFullyRead;
    private final int maxPortion;
    private final boolean isFinite;

    private int array;
    private int position;
    private boolean isOpen;

    public MockReadableByteChannel(byte[][] data, int maxPortion, boolean isFinite) {
        this.data = data;
        this.maxPortion = maxPortion;
        this.isFinite = isFinite;

        array = 0;
        position = 0;
        isOpen = true;

        int readsToFullyRead = 0;
        for (byte[] chunk : data) {
            readsToFullyRead += (chunk.length + 1);
        }

        this.readsToFullyRead = readsToFullyRead;
    }

    public void close() {
        isOpen = false;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void proceed() {
        if (array < data.length && data[array].length - position == 0) {
            array++;
            position = 0;
        }
    }

    public int read(ByteBuffer destination) throws IOException {
        int length = data.length;
        if (array == length) {
            if (isFinite) {
                return -1;
            } else {
                return 0;
            }
        } else {
            int remainingData = data[array].length - position;
            if (remainingData == 0) {
                if (isFinite && array == length - 1) {
                    return -1;
                } else {
                    return 0;
                }
            } else {
                int remainingBuffer = destination.remaining();
                int portion = Math.min(Math.min(remainingBuffer, remainingData), maxPortion);

                for (int i = 0; i < portion; i++) {
                    destination.put(data[array][position]);

                    position++;
                }

                return portion;
            }
        }
    }

    public int readsToFullyRead() {
        return readsToFullyRead;
    }
}
