package ru.example.test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import org.apache.log4j.Logger;

public class Main {
    private static final Logger logger;

    static {
        logger = Logger.getLogger(Main.class);
    }

    public static void main(String[] args) {
        logger.info("Started testing.");

        Result result = JUnitCore.runClasses(
                TestRequest.class
        );

        int runCount = result.getRunCount();

        if (result.wasSuccessful()) {
            logger.info(runCount + " test(s) successfully passed.");

            System.exit(0);
        } else {
            for (Failure failure : result.getFailures()) {
                logger.error(failure.getDescription(), failure.getException());
            }

            logger.info(result.getFailureCount() + " of " + runCount + " test(s) failed.");

            System.exit(1);
        }
    }
}
