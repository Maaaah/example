package ru.example;

import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public class Accepter extends Thread {
    private final ServerSocketChannel serverChannel;
    private final Selector selector;
    private final Worker[] workers;

    public Accepter(ServerSocketChannel serverChannel, Processor processor, TweakProcessor tweak,
                    int workerCount, String name)
            throws Exception
    {
        this.serverChannel = serverChannel;

        this.selector = Selector.open();

        this.workers = new Worker[workerCount];
        for (int i = 0; i < workerCount; i++) {
            Worker worker = new Worker(Selector.open(), processor);

            this.workers[i] = worker;

            tweak.register(new String[]{"http", name, "worker-" + (i < 10 ? "0" + i : i)}, worker);
        }
    }

    public void run() {
        try {
            for (Worker worker : workers) {
                worker.start();
            }

            serverChannel.register(selector, SelectionKey.OP_ACCEPT);

            int i = 0;
            for (;;) {
                selector.select();

                Iterator<SelectionKey> keysIterator = selector.selectedKeys().iterator();
                while (keysIterator.hasNext()) {
                    keysIterator.next();
                    keysIterator.remove();

                    SocketChannel clientChannel;
                    while ((clientChannel = serverChannel.accept()) != null) {
                        clientChannel.configureBlocking(false);

                        workers[i].connectionAccepted(clientChannel);

                        i++;
                        if (i == workers.length) {
                            i = 0;
                        }
                    }
                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
