package ru.example;

public class Http {
    public static boolean isConnectionCloseHeaderName(
            int headerNameStart, int headerNameEnd, CircularBuffer buffer)
    {
        if (headerNameEnd - headerNameStart == 10) {
            byte b = buffer.get(headerNameStart);
            if (b == 0x43 || b == 0x63) { // 'C' or 'c'
                b = buffer.get(headerNameStart + 1);
                if (b == 0x4F || b == 0x6F) { // 'O' or 'o'
                    b = buffer.get(headerNameStart + 2);
                    if (b == 0x4E || b == 0x6E) { // 'N' or 'n'
                        b = buffer.get(headerNameStart + 3);
                        if (b == 0x4E || b == 0x6E) { // 'N' or 'n'
                            b = buffer.get(headerNameStart + 4);
                            if (b == 0x45 || b == 0x65) { // 'E' or 'e'
                                b = buffer.get(headerNameStart + 5);
                                if (b == 0x43 || b == 0x63) { // 'C' or 'c'
                                    b = buffer.get(headerNameStart + 6);
                                    if (b == 0x54 || b == 0x74) { // 'T' or 't'
                                        b = buffer.get(headerNameStart + 7);
                                        if (b == 0x49 || b == 0x69) { // 'I' or 'i'
                                            b = buffer.get(headerNameStart + 8);
                                            if (b == 0x4F || b == 0x6F) { // 'O' or 'o'
                                                b = buffer.get(headerNameStart + 9);
                                                if (b == 0x4E || b == 0x6E) { // 'N' or 'n'
                                                    return true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public static int parseContentLengthHeaderValue(
            int headerValueStart, int headerValueEnd, CircularBuffer buffer)
    {
        int contentLength = 0;
        for (int i = skipSpaces(headerValueStart, headerValueEnd, buffer); i < headerValueEnd; i++) {
            byte digit = buffer.get(i);
            if (digit >= 0x30 && digit <= 0x39) {
                contentLength = contentLength * 10 + (digit - 0x30);
            } else if (skipSpaces(i, headerValueEnd, buffer) < headerValueEnd) {
                return -1;
            }
        }

        return contentLength;
    }

    public static boolean parseConnectionCloseHeaderValue(
            int headerValueStart, int headerValueEnd, CircularBuffer buffer)
    {
        int i = skipSpaces(headerValueStart, headerValueEnd, buffer);
        if (headerValueEnd - i >= 5) {
            byte b = buffer.get(i);
            if (b == 0x63) { // 'c'
                b = buffer.get(i + 1);
                if (b == 0x6C) { // 'l'
                    b = buffer.get(i + 2);
                    if (b == 0x6F) { // 'o'
                        b = buffer.get(i + 3);
                        if (b == 0x73) { // 's'
                            b = buffer.get(i + 4);
                            if ((b == 0x65) // 'e'
                                    && skipSpaces(i + 5, headerValueEnd, buffer) == headerValueEnd)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean isContentLengthHeaderName(
            int headerNameStart, int headerNameEnd, CircularBuffer buffer)
    {
        if (headerNameEnd - headerNameStart == 14) {
            byte b = buffer.get(headerNameStart);
            if (b == 0x43 || b == 0x63) { // 'C' or 'c'
                b = buffer.get(headerNameStart + 1);
                if (b == 0x4F || b == 0x6F) { // 'O' or 'o'
                    b = buffer.get(headerNameStart + 2);
                    if (b == 0x4E || b == 0x6E) { // 'N' or 'n'
                        b = buffer.get(headerNameStart + 3);
                        if (b == 0x54 || b == 0x74) { // 'T' or 't'
                            b = buffer.get(headerNameStart + 4);
                            if (b == 0x45 || b == 0x65) { // 'E' or 'e'
                                b = buffer.get(headerNameStart + 5);
                                if (b == 0x4E || b == 0x6E) { // 'N' or 'n'
                                    b = buffer.get(headerNameStart + 6);
                                    if (b == 0x54 || b == 0x74) { // 'T' or 't'
                                        b = buffer.get(headerNameStart + 7);
                                        if (b == 0x2D) { // '-'
                                            b = buffer.get(headerNameStart + 8);
                                            if (b == 0x4C || b == 0x6C) { // 'L' or 'l'
                                                b = buffer.get(headerNameStart + 9);
                                                if (b == 0x45 || b == 0x65) { // 'E' or 'e'
                                                    b = buffer.get(headerNameStart + 10);
                                                    if (b == 0x4E || b == 0x6E) { // 'G' or 'g'
                                                        b = buffer.get(headerNameStart + 11);
                                                        if (b == 0x47 || b == 0x67) { // 'G' or 'g'
                                                            b = buffer.get(headerNameStart + 12);
                                                            if (b == 0x54 || b == 0x74) { // 'T' or 't'
                                                                b = buffer.get(headerNameStart + 13);
                                                                if (b == 0x48 || b == 0x68) { // 'H' or 'h'
                                                                    return true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public static int skipSpaces(int from, int to, CircularBuffer buffer) {
        int i = from;
        while (i < to && (buffer.get(i) == 32 || buffer.get(i) == 9)) {
            i++;
        }

        return i;
    }
}
