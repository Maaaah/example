package ru.example;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.GatheringByteChannel;

public class Response {
    private static final ByteBuffer EMPTY;

    private static final ByteBuffer _200;
    private static final ByteBuffer _204;
    private static final ByteBuffer _400;
    private static final ByteBuffer _404;
    private static final ByteBuffer _500;
    private static final ByteBuffer _501;
    private static final ByteBuffer _503;

    private static final ByteBuffer HTTP10;
    private static final ByteBuffer HTTP11;

    private static final ByteBuffer CONNECTIONCLOSE;
    private static final ByteBuffer CONTENTLENGTH;
    private static final ByteBuffer CONTENTTYPETEXTHTML;
    private static final ByteBuffer CONTENTTYPEAPPLICATIONOCTETSTREAM;

    private static final String SERVER = "Server: Example";

    static {
        try {
            EMPTY = ByteBuffer.allocateDirect(0);

            HTTP10 = Util.makeBuffer("HTTP/1.0 ");
            HTTP11 = Util.makeBuffer("HTTP/1.1 ");

            _503 = Util.makeBuffer("503 Service Unavailable" + Util.CRLF + SERVER + Util.CRLF);
            _501 = Util.makeBuffer("501 Not Implemented" + Util.CRLF + SERVER + Util.CRLF);
            _500 = Util.makeBuffer("500 Internal Server Error" + Util.CRLF + SERVER + Util.CRLF);
            _404 = Util.makeBuffer("404 Not Found" + Util.CRLF + SERVER + Util.CRLF);
            _400 = Util.makeBuffer("400 Bad Request" + Util.CRLF + SERVER + Util.CRLF);
            _204 = Util.makeBuffer("204 No Content" + Util.CRLF + SERVER + Util.CRLF);
            _200 = Util.makeBuffer("200 OK" + Util.CRLF + SERVER + Util.CRLF);

            CONNECTIONCLOSE = Util.makeBuffer("Connection: close" + Util.CRLF);
            CONTENTTYPETEXTHTML = Util.makeBuffer("Content-Type: text/html" + Util.CRLF);
            CONTENTTYPEAPPLICATIONOCTETSTREAM = Util.makeBuffer("Content-Type: application/octet-stream" + Util.CRLF);
            CONTENTLENGTH = Util.makeBuffer("Content-Length: ");
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();

            throw new IllegalStateException(ex);
        }
    }

    private static Response response(ByteBuffer code, ByteBuffer headers, ByteBuffer[] body,
                                     boolean isHTTP11, boolean isLast)
    {
        int bodyBufferCount = body.length;

        ByteBuffer[] response = new ByteBuffer[7 + bodyBufferCount];

        response[0] = isHTTP11 ? HTTP11.slice() : HTTP10.slice();
        response[1] = code.slice();
        response[2] = isLast ? CONNECTIONCLOSE.slice() : EMPTY.slice();
        response[3] = CONTENTLENGTH.slice();

        response[5] = headers.slice();
        response[6] = Util.END.slice();

        ByteBuffer lastBuffer = response[6];
        int bodyLength = 0;
        for (int i = 0; i < bodyBufferCount; i++) {
            ByteBuffer bodyPart = body[i].slice();

            int bodyPartLength = bodyPart.remaining();
            if (bodyPartLength > 0) {
                bodyLength += bodyPartLength;

                lastBuffer = bodyPart;
            }

            response[i + 7] = bodyPart;
        }

        response[4] = Util.makeBuffer(bodyLength);

        return new Response(response, lastBuffer);
    }

    public static Response response(boolean isHTTP11, boolean isLast, Content content) {
        return response(content.code, content.type.bytes, content.body, isHTTP11, isLast);
    }

    public static class Content {
        public static Content OK;
        public static Content NO_CONTENT;
        public static Content BAD_REQUEST;
        public static Content NOT_FOUND;
        public static Content INTERNAL_SERVER_ERROR;
        public static Content NOT_IMPLEMENTED;
        public static Content SERVICE_UNAVAILABLE;

        static {
            OK = new Content(_200);
            NO_CONTENT = new Content(_204);
            BAD_REQUEST = new Content(_400);
            NOT_FOUND = new Content(_404);
            INTERNAL_SERVER_ERROR = new Content(_500);
            NOT_IMPLEMENTED = new Content(_501);
            SERVICE_UNAVAILABLE = new Content(_503);
        }

        public static enum Type {
            NOCONTENT(EMPTY),
            TEXTHTML(CONTENTTYPETEXTHTML),
            APPLICATIONOCTETSTREAM(CONTENTTYPEAPPLICATIONOCTETSTREAM);

            private final ByteBuffer bytes;

            private Type(ByteBuffer bytes) {
                this.bytes = bytes;
            }
        }

        private final ByteBuffer code;
        private final Type type;
        private final ByteBuffer[] body;

        public Content(Type type, ByteBuffer... body) {
            this.code = _200;
            this.type = type;
            this.body = body;
        }

        private Content(ByteBuffer code) {
            this.code = code;
            this.type = Type.NOCONTENT;
            this.body = new ByteBuffer[0];
        }
    }

    private final ByteBuffer[] buffers;
    private final ByteBuffer last;

    private Response(ByteBuffer[] buffers, ByteBuffer last) {
        this.buffers = buffers;
        this.last = last;
    }

    public boolean write(GatheringByteChannel channel) throws IOException {
        channel.write(buffers);

        return last.remaining() == 0;
    }
}
