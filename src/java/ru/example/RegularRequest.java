package ru.example;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

public class RegularRequest implements Request {
    private static enum State { INITIAL, CR, CRLF, CRLFCR, BODY }

    private final CircularBuffer buffer;

    private State state;
    private boolean isEOF;
    private boolean isOverflow;
    private boolean isError;
    private boolean isGet;
    private boolean isPost;
    private boolean isLast;
    private boolean isHTTP11;
    private int start;
    private int position;
    private int end;
    private int methodEnd;
    private int requestURIQuestionMark;
    private int requestURIEnd;
    private int protocolEnd;
    private int headerLineStart;
    private int headerLineColon;
    private int contentLength;
    private int bodyStart;
    private int readsMade;
    private int positiveLengthReadsMade;
    private ByteBuffer data;
    private ByteBuffer URI;
    private ByteBuffer body;

    public RegularRequest(ByteBuffer buffer) {
        buffer.clear();

        this.buffer = new CircularBuffer(buffer);

        start = 0;
        position = 0;
        end = 0;
        isEOF = false;

        reset();
    }

    public void reset() {
        state = State.INITIAL;
        isLast = false;
        isOverflow = false;
        isError = false;
        isGet = false;
        isPost = false;
        isHTTP11 = false;
        methodEnd = -1;
        requestURIQuestionMark = -1;
        requestURIEnd = -1;
        protocolEnd = -1;
        headerLineStart = -1;
        headerLineColon = -1;
        contentLength = -1;
        bodyStart = -1;
        data = null;
        URI = null;
        body = null;

        buffer.shrink(start);
    }

    public boolean hasPendingData() {
        return position < end;
    }

    public int readsMade() {
        return readsMade;
    }

    public int positiveLengthReadsMade() {
        return positiveLengthReadsMade;
    }

    public boolean read(ReadableByteChannel channel) throws IOException {
        readsMade = 0;
        positiveLengthReadsMade = 0;

        if (position == end) {
            if (isEOF) {
                return false;
            } else {
                while (true) {
                    int bytes = buffer.read(channel);

                    readsMade++;

                    if (bytes > 0) {
                        positiveLengthReadsMade++;
                    } else {
                        isEOF = (bytes < 0);

                        break;
                    }
                }

                end = buffer.position();
            }
        }

        // This is true when EOF is sent separately
        if (position == end) {
            return false;
        }

        while (true) {
            if (state == State.BODY) {
                int bodyEnd = bodyStart + contentLength;
                if (end >= bodyEnd) {
                    // End of the body
                    buffer.copyTo(data, position, bodyEnd - position);

                    body = Buffer.slice(requestURIEnd - methodEnd - 1, requestURIEnd - methodEnd - 1 + contentLength, data);

                    position = bodyEnd;
                    start = position;

                    if (position == end && isEOF) {
                        isLast = true;
                    }

                    buffer.shrink(position);

                    return true;
                } else {
                    buffer.copyTo(data, position, end - position);

                    position = end;

                    buffer.shrink(position);
                }
            }

            if (position == end) {
                break;
            }

            byte b = buffer.get(position);

            switch (state) {
                case INITIAL:
                    if (b == 0x0D) {
                        state = State.CR;
                    } else if (protocolEnd == -1) {
                        if (b == 32) {
                            if (methodEnd == -1) {
                                methodEnd = position;
                            } else if (requestURIEnd == -1) {
                                requestURIEnd = position;
                            } else {
                                // Malformed Request-Line
                                isError = true;

                                return true;
                            }
                        } else if (b == 0x3F) { // '?'
                            if (requestURIEnd == -1 && requestURIQuestionMark == -1) {
                                requestURIQuestionMark = position;
                            }
                        }
                    } else if (b == 0x3A) { // ':'
                        if (headerLineColon == -1) {
                            headerLineColon = position;
                        }
                    }

                    break;

                case CR:
                    if (b == 0x0A) {
                        if (protocolEnd == -1) {
                            if (methodEnd == -1 || requestURIEnd == -1) {
                                // Malformed Request-Line
                                isError = true;

                                return true;
                            } else {
                                protocolEnd = position - 1;
                            }
                        }

                        if (headerLineStart == -1) {
                            headerLineStart = position + 1;
                        } else {
                            if (Http.isContentLengthHeaderName(
                                    headerLineStart, headerLineColon, buffer)) {
                                contentLength = Http.parseContentLengthHeaderValue(
                                        headerLineColon + 1, position - 1, buffer);
                                if (contentLength == -1) {
                                    // Malformed Content-Length
                                    isError = true;

                                    return true;
                                }
                            } else if (!isLast && Http.isConnectionCloseHeaderName(
                                    headerLineStart, headerLineColon, buffer)) {
                                isLast = Http.parseConnectionCloseHeaderValue(
                                        headerLineColon + 1, position - 1, buffer);
                            }

                            headerLineStart = position + 1;
                            headerLineColon = -1;
                        }

                        state = State.CRLF;
                    } else {
                        isError = true;

                        return true;
                    }

                    break;

                case CRLF:
                    if (b == 0x0D) {
                        state = State.CRLFCR;
                    } else {
                        state = State.INITIAL;
                    }

                    break;

                case CRLFCR:
                    if (b == 0x0A) {
                        if (protocolEnd - requestURIEnd == 9
                                && buffer.get(requestURIEnd + 1) == 0x48  // 'H'
                                && buffer.get(requestURIEnd + 2) == 0x54  // 'T'
                                && buffer.get(requestURIEnd + 3) == 0x54  // 'T'
                                && buffer.get(requestURIEnd + 4) == 0x50  // 'P'
                                && buffer.get(requestURIEnd + 5) == 0x2F  // '/'
                                && buffer.get(requestURIEnd + 6) == 0x31  // '1'
                                && buffer.get(requestURIEnd + 7) == 0x2E)  // '.'
                        {
                            byte b0 = buffer.get(requestURIEnd + 8);
                            if (b0 == 0x30) {  // '0' or ...
                                isLast = true;
                            } else if (b0 == 0x31) { // ... '1'
                                isHTTP11 = true;
                            } else {
                                // Wrong protocol
                                isError = true;

                                return true;
                            }
                        } else {
                            isError = true;

                            return true;
                        }

                        boolean hasBody = (contentLength != -1);
                        int URIlength = requestURIEnd - methodEnd - 1;
                        if (hasBody) {
                            bodyStart = position + 1;

                            data = ByteBuffer.allocate(URIlength + contentLength);

                            state = State.BODY;
                        } else {
                            data = ByteBuffer.allocate(URIlength);
                        }

                        buffer.copyTo(data, methodEnd + 1, URIlength);

                        URI = Buffer.slice(0, URIlength, data);

                        if (methodEnd == start + 3 && buffer.get(start) == 0x47
                                && buffer.get(start + 1) == 0x45 && buffer.get(start + 2) == 0x54) {
                            isGet = true;

                            if (hasBody) {
                                isError = true;

                                return true;
                            } else {
                                position++;
                                start = position;

                                if (position == end && isEOF) {
                                    isLast = true;
                                }

                                return true;
                            }
                        } else if (methodEnd == start + 4 && buffer.get(start) == 0x50
                                && buffer.get(start + 1) == 0x4F && buffer.get(start + 2) == 0x53
                                && buffer.get(start + 3) == 0x54) {
                            isPost = true;

                            if (!hasBody) {
                                isError = true;

                                return true;
                            }
                        } else {
                            // Method not supported
                            isError = true;

                            return true;
                        }
                    } else {
                        isError = true;

                        return true;
                    }

                    break;
            }

            position++;
        }

        if (isEOF) {
            isError = true;

            return true;
        } else if (buffer.remaining() == 0) {
            // buffer overflow - cant happen if the request has body
            isOverflow = true;

            return true;
        } else {
            return false;
        }
    }

    public boolean isHTTP11() {
        return isHTTP11;
    }

    public boolean isEOF() {
        return isEOF;
    }

    public boolean isLast() {
        return isLast;
    }

    public boolean isOverflow() {
        return isOverflow;
    }

    public boolean isError() {
        return isError;
    }

    public boolean isGet() {
        return isGet;
    }

    public boolean isPost() {
        return isPost;
    }

    public ByteBuffer URI() {
        return URI;
    }

    public ByteBuffer body() {
        return body;
    }
}
