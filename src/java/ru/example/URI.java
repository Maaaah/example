package ru.example;

import java.net.MalformedURLException;
import java.nio.ByteBuffer;

public class URI {
    private final ByteBuffer buffer;

    private boolean isEncoded;

    public URI(ByteBuffer buffer) {
        this.buffer = buffer;
    }

    public boolean isEncoded() {
        return isEncoded;
    }

    public char next() throws MalformedURLException {
        byte b = buffer.get();

        char c = 0;
        if (b == 0x25) {
            for (int i = 0; i < 2; i++) {
                if (buffer.remaining() == 0) {
                    throw new MalformedURLException();
                } else {
                    c <<= 4;

                    byte b0 = buffer.get();
                    if (b0 >= 0x30 && b0 <= 0x39) {
                        b0 -= 0x30;
                    } else if (b0 >= 0x41 && b0 <= 0x46) {
                        b0 -= 0x37;
                    } else if (b0 >= 0x61 && b0 <= 0x66) {
                        b0 -= 0x57;
                    } else {
                        throw new MalformedURLException();
                    }

                    c += (char) (b0 & 0xFF);
                }
            }

            isEncoded = true;
        } else {
            c = (char) (b & 0xFF);

            isEncoded = false;
        }

        return c;
    }

    public boolean hasNext() {
        return buffer.remaining() > 0;
    }
}
