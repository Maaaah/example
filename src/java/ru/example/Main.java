package ru.example;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.channels.ServerSocketChannel;

public class Main {
    public static void main(String[] args) throws Exception {
        TweakProcessor tweak = new TweakProcessor();

        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);

        ServerSocket serverSocket = serverChannel.socket();
        serverSocket.bind(new InetSocketAddress("0.0.0.0", 8686), 1024);

        ReadmeFileProcessor readmeFileProcessor = new ReadmeFileProcessor();

        for (int i = 0; i < 2; i++) {
            new Accepter(serverChannel, readmeFileProcessor, tweak, 16, "readme-" + i).start();
        }

        serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);

        serverSocket = serverChannel.socket();
        serverSocket.bind(new InetSocketAddress("0.0.0.0", 8787), 1024);

        new Accepter(serverChannel, tweak, tweak, 1, "tweak").start();
    }
}