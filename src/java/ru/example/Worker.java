package ru.example;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Worker extends Thread implements TweakProcessor.Source {
    private final Selector selector;
    private final ConcurrentLinkedQueue<Task> events;
    private final Processor processor;

    private volatile long time;

    private Exchange head;
    private Exchange tail;

    private int readsMade;
    private int positiveLengthReadsMade;
    private int requestsReceived;
    private int connectionsOpen;
    private int connectionsReset;
    private int pipesBroken;

    public interface Context {
        public void respond(Response.Content content);
    }

    private class Exchange {
        private final SelectionKey key;

        private ByteBuffer buffer;

        private Request request;
        private Response response;
        private long time;

        private Exchange previous;
        private Exchange next;

        private boolean isReading;
        private boolean isHTTP11;
        private boolean closeConnection;

        private Exchange(SelectionKey key) {
            this.key = key;

            isHTTP11 = false;
            closeConnection = false;
            isReading = true;
        }

        private void start() {
            time = System.currentTimeMillis();

            if (buffer == null) {
                // TODO: restrict allocated memory by pooling
                buffer = ByteBuffer.allocate(32768);
            }

            request = new RegularRequest(buffer);

            key.interestOps(SelectionKey.OP_READ);

            add();
        }

        private void add() {
            if (head == null) {
                head = this;
                tail = this;
            } else {
                tail.next = this;
                previous = tail;
                tail = this;
            }
        }

        private void proceed() {
            SocketChannel channel = (SocketChannel) key.channel();

            for (;;) {
                boolean done;

                if (isReading) {
                    try {
                        done = request.read(channel);
                    } catch (IOException ex) {
                        String message = ex.getMessage();
                        if ("Connection reset by peer".equals(message)) {
                            connectionsReset++;
                        } else if ("Broken pipe".equals(message)) {
                            pipesBroken++;
                        } else {
                            ex.printStackTrace();
                        }

                        finish();

                        break;
                    }

                    readsMade += request.readsMade();
                    positiveLengthReadsMade += request.positiveLengthReadsMade();

                    if (done) {
                        isHTTP11 = request.isHTTP11();

                        if (buffer == null) {
                            response = Response.response(isHTTP11, true, Response.Content.SERVICE_UNAVAILABLE);

                            closeConnection = true;
                        } else if (request.isOverflow()) {
                            response = Response.response(isHTTP11, true, Response.Content.BAD_REQUEST);

                            closeConnection = true;
                        } else if (request.isError()) {
                            response = Response.response(isHTTP11, true, Response.Content.NOT_IMPLEMENTED);

                            closeConnection = true;
                        } else {
                            requestsReceived++;

                            ByteBuffer responseBuffer;
                            if (((RegularRequest) request).hasPendingData()) {
                                responseBuffer = null;
                            } else {
                                // TODO: reuse request buffer if its fully drained something like
                                // TODO: responseBuffer = buffer.slice().clear();
                                responseBuffer = null;
                            }

                            final Thread workerThread = Thread.currentThread();
                            final Response.Content[] synchronousResponse = new Response.Content[]{null};
                            try {
                                processor.process(new Context() {
                                    @Override
                                    public void respond(Response.Content content) {
                                        if (Thread.currentThread() == workerThread) {
                                            synchronousResponse[0] = content;
                                        } else {
                                            schedule(new WriteDeferredResponse(key, content));
                                        }
                                    }
                                }, (RegularRequest) request, responseBuffer);

                                closeConnection = request.isLast();

                                Response.Content content = synchronousResponse[0];
                                if (content != null) {
                                    response = Response.response(isHTTP11, closeConnection, content);
                                }
                            } catch (Throwable th) {
                                th.printStackTrace();

                                response = Response.response(isHTTP11, true, Response.Content.INTERNAL_SERVER_ERROR);

                                closeConnection = true;
                            }
                        }

                        if (response == null) {
                            key.interestOps(0);
                        } else {
                            key.interestOps(SelectionKey.OP_WRITE);
                        }

                        remove();

                        add();

                        time = System.currentTimeMillis();

                        isReading = false;

                        // TODO: Do not break here if response is not null, but reset isReading
                        // TODO: to write immediately w/o waiting for OP_WRITE
                        break;
                    } else if (request.isEOF()) {
                        finish();

                        break;
                    } else {
                        key.interestOps(SelectionKey.OP_READ);

                        break;
                    }
                } else {
                    try {
                        done = response.write(channel);
                    } catch (IOException ex) {
                        String message = ex.getMessage();
                        if ("Connection reset by peer".equals(message)) {
                            connectionsReset++;
                        } else if ("Broken pipe".equals(message)) {
                            pipesBroken++;
                        } else {
                            ex.printStackTrace();
                        }

                        finish();

                        break;
                    }

                    if (done) {
                        response = null;

                        if (closeConnection) {
                            finish();

                            break;
                        } else {
                            request.reset();

                            isReading = true;
                        }
                    } else {
                        break;
                    }
                }
            }
        }

        private void respond(Response.Content content) throws IOException {
            this.response = Response.response(isHTTP11, closeConnection, content);

            key.interestOps(SelectionKey.OP_WRITE);
        }

        private void remove() {
            if (previous == null) {
                head = next;
            } else {
                previous.next = next;
            }
            if (next == null) {
                tail = previous;
            } else {
                next.previous = previous;
            }
        }

        private void finish() {
            remove();

            buffer = null;

            key.cancel();

            // If this socket has an associated channel then the channel is closed as well.
            try {
                ((SocketChannel) key.channel()).socket().close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            connectionsOpen--;
        }
    }

    private static interface Task {
        public void run() throws IOException;
    }

    private class ConnectionAccepted implements Task {
        private final SocketChannel channel;
        private final Selector selector;

        private ConnectionAccepted(SocketChannel channel, Selector selector) {
            this.channel = channel;
            this.selector = selector;
        }

        public void run() throws IOException {
            connectionsOpen++;

            SelectionKey key = channel.register(selector, 0);

            Exchange exchange = new Exchange(key);

            key.attach(exchange);

            exchange.start();
        }
    }

    private static class WriteDeferredResponse implements Task {
        private final SelectionKey key;
        private final Response.Content content;

        private WriteDeferredResponse(SelectionKey key, Response.Content content) {
            this.key = key;
            this.content = content;
        }

        public void run() throws IOException {
            ((Exchange) key.attachment()).respond(content);
        }
    }

    private class FlushStatistics implements Task {
        private final TreeMap<String, Integer> values;

        private FlushStatistics() {
            this.values = new TreeMap<String, Integer>();
        }

        public void run() {
            synchronized (Worker.this) {
                values.put("readsMade", readsMade);
                values.put("positiveLengthReadsMade", positiveLengthReadsMade);
                values.put("connectionsOpen", connectionsOpen);
                values.put("connectionsReset", connectionsReset);
                values.put("pipesBroken", pipesBroken);
                values.put("requestsReceived", requestsReceived);

                Worker.this.notifyAll();
            }
        }
    }

    private void schedule(Task task) {
        events.offer(task);

        selector.wakeup();
    }

    public TreeSet<String> children(ArrayList<String> path) {
        return new TreeSet<String>();
    }

    public TreeMap<String, ?> values(ArrayList<String> path) {
        FlushStatistics task = new FlushStatistics();

        if (Thread.currentThread() == this) {
            task.run();
        } else {
            synchronized (this) {
                schedule(task);

                try {
                    wait();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return task.values;
    }

    public Worker(Selector selector, Processor processor) {
        this.selector = selector;
        this.processor = processor;

        time = 0;

        events = new ConcurrentLinkedQueue<Task>();

        readsMade = 0;
        requestsReceived = 0;
        connectionsOpen = 0;
        connectionsReset = 0;
        pipesBroken = 0;
    }

    public void connectionAccepted(SocketChannel channel) {
        schedule(new ConnectionAccepted(channel, selector));
    }

    public void run() {
        try {
            for (;;) {
                selector.select();

                Task event;
                while ((event = events.poll()) != null) {
                    event.run();
                }

                Iterator<SelectionKey> keysIterator = selector.selectedKeys().iterator();
                while (keysIterator.hasNext()) {
                    SelectionKey key = keysIterator.next();

                    keysIterator.remove();

                    ((Exchange) key.attachment()).proceed();
                }

                // Close timed out connections
                // TODO: check not on every request, but periodically
                long currentTime = time;
                if (currentTime > 0) {
                    Exchange current = head;
                    while (current != null && currentTime - current.time >= 300000L) {
                        current.finish();

                        current = current.next;
                    }

                    time = 0;
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
