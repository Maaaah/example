package ru.example;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ReadmeFileProcessor implements Processor {
    private final ExecutorService executor;

    public ReadmeFileProcessor() {
        executor = Executors.newSingleThreadExecutor();
    }

    @Override
    public void process(final Worker.Context context, RegularRequest request, ByteBuffer buffer) throws Exception {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    byte[] content = Files.readAllBytes(Paths.get("README.txt"));

                    context.respond(new Response.Content(
                            Response.Content.Type.TEXTHTML, ByteBuffer.wrap("<html><body>".getBytes("ISO-8859-1")),
                            ByteBuffer.wrap(content), ByteBuffer.wrap("</body></html>".getBytes("ISO-8859-1"))));
                } catch (IOException ex) {
                    context.respond(Response.Content.INTERNAL_SERVER_ERROR);
                }
            }
        });
    }
}
