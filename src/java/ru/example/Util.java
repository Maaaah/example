package ru.example;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.LinkedHashSet;

public class Util {
    public static final String CRLF = "\r\n";
    public static final ByteBuffer END;

    private static final ByteBuffer[] NUMBERS;

    static {
        try {
            END = Util.makeBuffer(Util.CRLF);

            NUMBERS = new ByteBuffer[32768];
            for (int i = 0; i < Util.NUMBERS.length; i++) {
                Util.NUMBERS[i] = Util.makeBuffer(Integer.toString(i) + Util.CRLF);
            }
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();

            throw new IllegalStateException(ex);
        }
    }

    public static String toString(ByteBuffer buffer) {
        if (buffer == null) {
            return "null";
        }

        StringBuilder stringBuilder = new StringBuilder("[ ");
        for (int i = 0; i < buffer.limit(); i++) {
            stringBuilder.append(' ');
            int b = ((int) buffer.get(i)) & 0xFF;
            if (b < 16) {
                stringBuilder.append('0');
            }
            stringBuilder.append(Integer.toHexString(b));
        }
        stringBuilder.append(" ]");

        return stringBuilder.toString();
    }

    public static <T>int indexOf(LinkedHashSet<T> set, T o) {
        int i = 0;
        for (Object element : set) {
            if (element == null) {
                if (o == null) {
                    return i;
                }
            } else if (element.equals(o)) {
                return i;
            }

            i++;
        }

        return -1;
    }

    public static ByteBuffer makeBuffer(String string, boolean isDirect) throws UnsupportedEncodingException {
        byte[] bytes = string.getBytes("ISO-8859-1");

        ByteBuffer buffer = isDirect ? ByteBuffer.allocateDirect(bytes.length) : ByteBuffer.allocate(bytes.length);
        buffer.put(bytes);
        buffer.clear();

        return buffer;
    }

    public static ByteBuffer makeBuffer(String string) throws UnsupportedEncodingException {
        return makeBuffer(string, true);
    }

    public static ByteBuffer makeBuffer(int number) {
        return makeBuffer(number, false);
    }

    public static ByteBuffer makeBuffer(int number, boolean isDirect) {
        if (number < NUMBERS.length) {
            return NUMBERS[number].slice();
        }

        int length = 0;
        int n = number;
        while (n > 0) {
            n = n / 10;
            length++;
        }

        ByteBuffer buffer = isDirect ? ByteBuffer.allocateDirect(length + 2) : ByteBuffer.allocate(length + 2);
        n = number;
        int i = length - 1;
        while (n > 0) {
            buffer.put(i, (byte) (n % 10 + '0'));
            n = n / 10;
            i--;
        }

        buffer.put(length, (byte) 0x0D);
        buffer.put(length + 1, (byte) 0x0A);

        return buffer;
    }
}
