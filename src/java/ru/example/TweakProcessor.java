package ru.example;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

public class TweakProcessor implements Processor {
    public static interface Source {
        public TreeSet<String> children(ArrayList<String> path);
        public TreeMap<String, ?> values(ArrayList<String> path);
    }

    public static class Directory {
        private static Directory mkdirs(Directory root, String[] path) {
            Directory current = root;
            for (String name : path) {
                Directory child = current.directories.get(name);

                if (child == null) {
                    child = new Directory();

                    current.directories.put(name, child);
                }

                current = child;
            }

            return current;
        }

        private final TreeMap<String, Directory> directories;

        private Source source;

        private Directory() {
            directories = new TreeMap<String, Directory>();
        }
    }

    private final Directory root;

    public TweakProcessor() {
        root = new Directory();
    }

    public void register(String[] path, Source source) {
        Directory directory = Directory.mkdirs(root, path);

        directory.source = source;
    }

    public void process(Worker.Context context, RegularRequest request, ByteBuffer buffer)
            throws MalformedURLException, UnsupportedEncodingException
    {
        URI URI = new URI(request.URI());
        ArrayList<String> names = null;
        ArrayList<String> dynamicNames = null;
        ArrayList<String> path = null;
        Directory directory = null;
        Source source = null;
        int i = 0;
        char c = 0;
        boolean isFirst = true;
        while (URI.hasNext()) {
            c = URI.next();

            if (c == '?' && !URI.isEncoded()) {
                break;
            }

            if (c == '/') {
                if (isFirst) {
                    directory = root;
                    source = directory.source;

                    path = new ArrayList<String>();

                    isFirst = false;
                } else {
                    for (Iterator<String> it = names.iterator(); it.hasNext();) {
                        String name = it.next();
                        int length = name.length();
                        if (i != length) {
                            it.remove();
                        }
                    }

                    for (Iterator<String> it = dynamicNames.iterator(); it.hasNext();) {
                        String name = it.next();
                        int length = name.length();
                        if (i != length) {
                            it.remove();
                        }
                    }

                    if (names.isEmpty()) {
                        if (dynamicNames.isEmpty()) {
                            path = null;

                            break;
                        } else {
                            String name = dynamicNames.get(0);
                            path.add(name);

                            directory = null;
                        }
                    } else {
                        String name = names.get(0);

                        directory = directory.directories.get(name);
                        source = directory.source;

                        path.add(name);
                    }
                }

                names = new ArrayList<String>();
                if (directory != null) {
                    names.addAll(directory.directories.keySet());
                }

                dynamicNames = new ArrayList<String>();
                if (source != null) {
                    dynamicNames.addAll(source.children(path));
                }

                i = 0;
            } else if (isFirst) {
                break;
            } else {
                for (Iterator<String> it = names.iterator(); it.hasNext();) {
                    String name = it.next();
                    int length = name.length();
                    if (i >= length || c != name.charAt(i)) {
                        it.remove();
                    }
                }

                for (Iterator<String> it = dynamicNames.iterator(); it.hasNext();) {
                    String name = it.next();
                    int length = name.length();
                    if (i >= length || c != name.charAt(i)) {
                        it.remove();
                    }
                }

                if (names.isEmpty() && dynamicNames.isEmpty()) {
                    path = null;

                    break;
                }

                i++;
            }
        }

        if (c != '/' || path == null) {
            context.respond(Response.Content.NOT_FOUND);

            return;
        }

        StringBuilder html = new StringBuilder();

        html.append("<html><head></head><body><hr/><h2>Tweak</h2><hr/><h3><a href=\"/\">root</a>");
        ArrayList<String> copy = new ArrayList<String>();
        for (String name : path) {
            copy.add(name);

            html.append("&nbsp;/&nbsp;<a href=\"");

            makeLink(copy, html);

            html.append("\">");
            html.append(name);
            html.append("</a>");
        }
        html.append("</h3><hr/>");

        int last = copy.size();
        TreeSet<String> children = new TreeSet<String>();
        if (directory != null) {
            children.addAll(directory.directories.keySet());
        }
        if (source != null) {
            children.addAll(source.children(path));
        }

        if (!children.isEmpty()) {
            html.append("<h3>");

            for (String name : children) {
                html.append("<a href=\"");

                copy.add(name);
                makeLink(copy, html);
                copy.remove(last);

                html.append("\">");
                html.append(name);
                html.append("</a><br/>");
            }

            html.append("</h3>");
        }

        html.append("<hr/>");

        if (source != null) {
            TreeMap<String, ?> values = source.values(path);

            if (!values.isEmpty()) {
                html.append("<h3><table>");

                for (Map.Entry<String, ?> entry : values.entrySet()) {
                    html.append("<tr><td style=\"white-space:nowrap; vertical-align:top\">");
                    html.append(entry.getKey());
                    html.append("</td><td>&nbsp;</td><td style=\"vertical-align:top\">");
                    html.append(entry.getValue());
                    html.append("</td></tr>");
                }

                html.append("</table></h3>");
            }
        }

        html.append("<hr/>");

        html.append("</body>");

        context.respond(new Response.Content(
                Response.Content.Type.TEXTHTML, ByteBuffer.wrap(html.toString().getBytes("ISO-8859-1"))));
    }

    private static void makeLink(ArrayList<String> names, StringBuilder sb) {
        sb.append('/');
        for (String name : names) {
            sb.append(name);
            sb.append('/');
        }
    }
}
