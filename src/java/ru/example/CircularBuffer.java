package ru.example;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

public class CircularBuffer {
    private final ByteBuffer buffer;
    private final ByteBuffer bufferView;
    private final int size;
    private int correction;
    private int start;
    private int end;
    private int remaining;

    public CircularBuffer(ByteBuffer buffer) {
        this.buffer = buffer;

        size = buffer.capacity();

        correction = 0;
        start = buffer.position();
        end = buffer.limit();
        remaining = size;

        buffer.clear();

        bufferView = buffer.slice();
    }

    public void copyTo(ByteBuffer destination, int start, int length) {
        int from = start - correction;
        int to = start + length - correction;

        ByteBuffer temporaryBuffer = bufferView.slice();
        if (from > size) {
            temporaryBuffer.position(from - size);
            temporaryBuffer.limit(from - size + length);

            destination.put(temporaryBuffer);
        } else {
            temporaryBuffer.position(from);

            if (to > size) {
                temporaryBuffer.limit(size);
                destination.put(temporaryBuffer);

                temporaryBuffer.position(0);
                temporaryBuffer.limit(length - size + from);
                destination.put(temporaryBuffer);
            } else {
                temporaryBuffer.limit(from + length);
                destination.put(temporaryBuffer);
            }
        }
    }

    public int read(ReadableByteChannel channel) throws IOException {
        int read = channel.read(buffer);
        if (read == -1) {
            return -1;
        }

        remaining -= read;
        end += read;

        int limit = buffer.limit();
        if (limit == size && buffer.position() == limit) {
            buffer.position(0);
            buffer.limit(start - correction);
        }

        return read;
    }

    public int position() {
        return end;
    }

    public int remaining() {
        return remaining;
    }

    public void shrink(int position) {
        remaining += (position - start);

        start = position;

        if (remaining == size) {
            end = position;
            correction = position;

            buffer.clear();
        } else {
            int limit = buffer.limit();
            if (limit < size) {
                int i = position - correction;
                if (i < size) { // CHECK IN UNIT TESTS!!!
                    buffer.limit(i);
                } else {
                    correction += size;

                    buffer.limit(size);
                }
            }
        }
    }

    public byte get(int position) {
        int i = position - correction;
        if (i < size) {
            return bufferView.get(i);
        } else {
            return bufferView.get(i - size);
        }
    }
}
