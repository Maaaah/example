package ru.example;

import java.io.IOException;
import java.nio.channels.ReadableByteChannel;

public interface Request {
    public void reset();

    public boolean read(ReadableByteChannel channel) throws IOException;

    public boolean isHTTP11();
    public boolean isEOF();
    public boolean isLast();
    public boolean isOverflow();
    public boolean isError();
    public boolean isGet();
    public boolean isPost();

    public int readsMade();
    public int positiveLengthReadsMade();
}
