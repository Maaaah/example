
This code is actually a very simple and lightweight web server implementation initially designed to serve requests of mobile board games.

The server supports both HTTP 1.0 and HTTP 1.1 (also with pipelining), both GET and POST requests (but limited size, i.e. no chunked encoding is supported).

The server utilizes plain Java threads and NIO as underlying engine.

When run (see below) the server responds to every GET or POST request sent to localhost:8686 with this file contents (see ReadmeFileProcessor.java).

Also some (very limited to the moment) diagnostics are available at localhost:8787.

Since the project is tiny no build system is used, just plain scripts.



On Windows:

To build:

Run .\bin\windows\build.bat

To run:

Run .\bin\windows\run.bat

To test:

Run .\bin\windows\run-unit-test.bat

To perform tests with coverage record (you will find the coverage report at ./target/cobertura/report/index.html):

Run .\bin\windows\run-unit-test.bat /R



On Linux:

To build:

Run ./bin/linux/build.sh

To run:

Run ./bin/linux/run.sh

To test:

Run ./bin/linux/run-unit-test.sh

To perform tests with coverage record (you will find the coverage report at ./target/cobertura/report/index.html):

Run ./bin/linux/run-unit-test.sh --report-coverage