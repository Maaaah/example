#!/bin/bash

cd `dirname $0`

rm -r ../../target/classes

mkdir -p ../../target/classes

javac -Xlint:unchecked -cp `find ../../lib -type f -name '*.jar' -printf %p":"` -d ../../target/classes `find ../../src/java -name *.java -printf %p" "`
