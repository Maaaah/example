#/bin/bash
cd `dirname $0`

CLASSPATH=`find ../../lib -type f -name '*.jar' -printf %p":"`
DATAFILE=../target/cobertura/cobertura.ser

rm -rf ../../target/* &&

mkdir -p ../../target/classes &&

javac -cp ${CLASSPATH} -g -d ../../target/classes `find ../../src/java -name *.java -printf %p" "`

if [ $? -ne 0 ];
then
    exit 1;
fi

if [ "$1" == "--report-coverage" ];
then
    mkdir -p ../../target/cobertura &&
    mkdir -p ../../target/cobertura/{classes-instrumented,report} &&

    java -cp ${CLASSPATH} net.sourceforge.cobertura.instrument.Main --datafile ${DATAFILE} --destination ../../target/cobertura/classes-instrumented `find ../../target/classes/ -path '../../target/classes/ru/example/test' -prune -o -name '*.class' -printf %p" "` >/dev/null 2>&1 &&

    java -noverify -cp ../../target/cobertura/classes-instrumented:../../target/classes:${CLASSPATH} -Dnet.sourceforge.cobertura.datafile=${DATAFILE} -Dlog4j.configuration=./unit-test-log4j.properties ru.example.test.Main &&

    java -cp ${CLASSPATH} net.sourceforge.cobertura.reporting.Main --datafile ${DATAFILE} --destination ../../target/cobertura/report --format html ../../src/java >/dev/null 2>&1 &&

    cd ../../target/cobertura/ &&

    zip -qr report.zip report/*
else
    java -cp ../../target/classes:${CLASSPATH} -Dlog4j.configuration=./unit-test-log4j.properties ru.example.test.Main
fi
