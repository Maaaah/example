@echo off

setlocal enabledelayedexpansion

cd /D %~dp0

cd ..\..\

set DATAFILE=%CD%\target\cobertura\cobertura.ser

rmdir /Q /S "target"

mkdir "target\classes"

set TESTCLASSES=%CD%\target\classes\ru\example\test

set TESTCLASSES=%TESTCLASSES:\=\\%

set SOURCEDIR=%CD%\src\java

set CLASSESDIR=%CD%\target\classes

set SOURCE=

for /F "usebackq delims=" %%N in (`dir /B /S src\java\*.java`) do (
    set LINE=%%N

    call set "LINE=%%LINE:%SOURCEDIR%\=%%" 

    set SOURCE="!LINE!" !SOURCE!
)

cd src\java

javac -encoding UTF-8 -cp ..\..\lib\*; -d ..\..\target\classes %SOURCE%

cd ..\..

set CLASSESTOINSTRUMENT=

for /F "usebackq delims=" %%N in (`dir /B /S target\classes\*.class ^| findstr /B /V /L /C:"%TESTCLASSES%"`) do (
    set LINE=%%N

    call set "LINE=%%LINE:%CLASSESDIR%\=%%"

    set CLASSESTOINSTRUMENT="!LINE!" !CLASSESTOINSTRUMENT!
)

IF "%1"=="/R" GOTO Reporting

    java -cp target\classes;lib\*; -Dlog4j.configuration=bin\unit-test-log4j.properties -Xmx1024m ru.example.test.Main

GOTO End

:Reporting

    mkdir target\cobertura

    mkdir target\cobertura\classes-instrumented
    
    mkdir target\cobertura\report

    cd target\classes

    java -noverify -cp ..\..\lib\*; net.sourceforge.cobertura.instrument.Main --datafile "%DATAFILE%" --destination ..\..\target\cobertura\classes-instrumented %CLASSESTOINSTRUMENT% >NUL 2>&1

    cd ..\..

    java -noverify -cp target\cobertura\classes-instrumented;target\classes;lib\*; -Dnet.sourceforge.cobertura.datafile="%DATAFILE%" -Dlog4j.configuration=bin\unit-test-log4j.properties ru.example.test.Main

    java -noverify -cp lib\*; net.sourceforge.cobertura.reporting.Main --datafile "%DATAFILE%" --destination target\cobertura\report --format html src\java >NUL 2>&1

:End
