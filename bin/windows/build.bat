@echo off

setlocal enabledelayedexpansion

cd /D %~dp0

rmdir /Q /S "..\..\target\classes"

mkdir "..\..\target\classes"

cd ..\..\src\java

set SOURCE=

for /F "usebackq delims=" %%N in (`dir /B /S /AD *`) do (
    set LINE="%%N"\*.java
    dir !LINE! >nul 2>&1
    IF NOT ERRORLEVEL 1 set SOURCE=!LINE! !SOURCE!
)

javac -encoding UTF-8 -cp ..\..\lib\*; -d ..\..\target\classes %SOURCE%
